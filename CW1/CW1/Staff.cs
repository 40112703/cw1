﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW1
{
   public class Staff
    {   
      private string FName;
      private string SName;
      private string Dept;
      private int Hoursworked;
      private int PayPence;
      private string DOB;
      private int ID;
      private double PayRate;


      //set_____() Saves the entered data to the correponding variable.
      //get_____() Displays the set data in the txt boxes.


      public void setName(String FN, String SN )
      {
          FName = FN;
          SName = SN;
      }
      public string getName(out String SurName)
      {
          SurName = SName;
          return FName;
      }

      public void setDOB(String DateOB)
      {
          DOB = DateOB;
      }
      public string getDOB()
      {
          return DOB;
      }

      public void setDept(String DT)
      {
         Dept = DT;
      }
      public string getDept()
      {
          return Dept;
      }

      public void setID(String IDs)
      {
          try
          {
              ID = Convert.ToInt32(IDs);
          }
          catch { }
      }
      public String getID()
      {
          return Convert.ToString(ID);
      } 

      public void setPayrate(String PR)
      {
          try
          {
              PayRate = Convert.ToDouble(PR);
          }
          catch { }
          PayPence = Convert.ToInt32(PayRate * 100.00);
      }
      public String getPayrate()
      {
          PayRate = Convert.ToDouble(PayPence);
          return  (PayRate / 100.00).ToString("####0.00");
      }

      public void setHours(String HR)
      {
          try
          {
              Hoursworked = Convert.ToInt32(HR);
          }
          catch { }
      }
      public String getHours()
      {
          return Convert.ToString(Hoursworked);
      }


       // get___Int() converts the String to an intager to be used in the validation checks

      public Int32 getIDInt()
      {
          return ID;
      }
      public Int32 getPayrateInt()
      {
          return PayPence;
      }
      public Int32 getHoursInt()
      {
          return Hoursworked;
      }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;




namespace CW1
{

    public partial class MainWindow : Window
    {
        private Staff staff;
  
        bool noErrors; // if true no errors occured during the validation check
        
        public MainWindow()
        {
            InitializeComponent();
            staff = new Staff();
        }

        private void checkValid()
        {
            if (txt_Fname.Text == "" || txt_Sname.Text == "" || txt_DOB.Text == "" || txt_Department.Text == "" || txt_ID.Text == "" || txt_Payrate.Text == "" || txt_Hoursworked.Text == "")
            {
                MessageBox.Show("ERROR: Please fill all fields");       // Checks all inputs have data. If not will display error message.
            }

            else if (staff.getIDInt() > 2000 || staff.getIDInt() < 1000)
            {
                MessageBox.Show("ERROR: Please enter a valid Staff ID");    // Checks if the staff ID entered is within the exepted range.
                txt_ID.Text = ("");
            }

            else if (staff.getPayrateInt() > 99999 || staff.getPayrateInt() < 0)
            {
                MessageBox.Show(" ERROR: Please enter a valid pay rate");    // Checks if the pay rate entered is within the exepted range.
                txt_Payrate.Text = ("");
            }

            else if (staff.getHoursInt() > 36 || staff.getHoursInt() < 1)
            {
                MessageBox.Show("ERROR: Please enter a valid number of hours");     // Checks if the hours worked entered is within the exepted range.
                txt_Hoursworked.Text = ("");
            }
            else
            {
                noErrors = true;    // If no error occur it returns true.
            }

        } 

        private void btn_Set_Click(object sender, RoutedEventArgs e)
        {
            // Sends the data from the text boxes to the variables

            staff.setName(txt_Fname.Text, txt_Sname.Text);
            staff.setDOB(txt_DOB.Text);
            staff.setDept(txt_Department.Text);
            staff.setID(txt_ID.Text);
            staff.setPayrate(txt_Payrate.Text);
            staff.setHours(txt_Hoursworked.Text);
            checkValid();
        }

        private void btn_Get_Click(object sender, RoutedEventArgs e)
        {

            // Sends the data from the variables to the text boxes

            String tempSName;

            txt_Fname.Text = staff.getName(out tempSName);
            txt_Sname.Text = tempSName;
            txt_DOB.Text = staff.getDOB();
            txt_Department.Text = staff.getDept();
            txt_ID.Text = staff.getID();
           txt_Payrate.Text = staff.getPayrate();
           txt_Hoursworked.Text = staff.getHours();
        }

        private void btn_Clear_Click(object sender, RoutedEventArgs e)
        {
            // Clears all data for textboxes.

            txt_Fname.Text = ("");
            txt_Sname.Text = ("");
            txt_DOB.Text = ("");
            txt_Department.Text = ("");
            txt_ID.Text = ("");
            txt_Payrate.Text = ("");
            txt_Hoursworked.Text = ("");
        }

        private void btn_CalcPay_Click(object sender, RoutedEventArgs e)
        {
            checkValid();
            if (noErrors == true)  // If no errors occured in the check opens the payslip window. 
            {
                PaySlip newWin = new PaySlip(staff);
                newWin.Show();
            }

        }

    }
}
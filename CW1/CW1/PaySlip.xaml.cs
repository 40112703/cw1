﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace CW1
{
    public partial class PaySlip : Window
    {

        

            int taxRate = 0;
            double netPay = 0;
            int grossPayPence = 0;
            double grossPay = 0;

        public PaySlip(Staff staff)
            {
                InitializeComponent();
                String tempSName;
                String tempFName;
                tempFName = staff.getName(out tempSName);
                



                txt_Name_Dep.Text = tempSName + ", " + tempFName + " (" + staff.getDept() + ")"; // Displays Surname, First name (Department).
                txt_Hoursworked2.Text = Convert.ToString(staff.getHoursInt());
                
               
                calcPay(staff);
                calcTaxRate();
                NetPay();

                txt_Grosspay.Text = "£" + grossPay.ToString("0.00");   //Displays the pay to two decimal places. 
                txt_Taxrate.Text = taxRate.ToString() + "%";
                txt_Netpay.Text = "£" + netPay.ToString("0.00");
            }



        private void btn_Close_Click(object sender, RoutedEventArgs e)
        {
            this.Close();   // Closes the Payslip window.
        }

        private void calcPay(Staff staff) 
        {
            grossPayPence = staff.getHoursInt() * staff.getPayrateInt(); // Multiplys number of hours worked by wage in pence.
            grossPay = Convert.ToDouble(grossPayPence) / 100;           // Then divides by 100 to give the wekks/months pay in pounds.
        }

        private void calcTaxRate()
        {
            if (grossPayPence < 100000)          // Checks if the pay gross pay is less than £10000 then takes off 10% tax.
            {
                taxRate =10;
            }
            else
            {                                     // If the gross pay is above £10000 it applies a 20% tax rate.
                taxRate =20;
            }
        }

        private void NetPay()
        {
            netPay = grossPay*(1-(taxRate * 0.01)); // Takes off tax to give net pay.
        }

    }
}

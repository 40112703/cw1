﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
      static void Main(string[] args)
        { 
            int area = 0;

            Console.WriteLine("Please the room name");
            String room = Console.ReadLine();

            Console.WriteLine("\nPlease enter the " + room + " width");
            string width = Console.ReadLine();

            int stringToInt1 = Convert.ToInt32(width);

            Console.WriteLine("\nPlease enter the " + room + " length");
            string length = Console.ReadLine();

            int stringToInt2 = Convert.ToInt32(length);

            area = stringToInt1 * stringToInt2;

            Console.WriteLine
                ("\nRoom         Size(sp ms)\n" + room +"         " + area + "\n");
            
            Console.WriteLine("Press return to exit");
            Console.ReadLine();
        }

    }
}
